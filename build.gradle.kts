plugins {

    id("com.android.application") version "8.1.0" apply false
    id("com.android.library") version "8.1.0" apply false
    kotlin("android") version "1.8.21" apply false
    kotlin("plugin.serialization") version "1.8.21" apply false
}

tasks.create<Delete>("clean") {
    delete(rootProject.buildDir)
}