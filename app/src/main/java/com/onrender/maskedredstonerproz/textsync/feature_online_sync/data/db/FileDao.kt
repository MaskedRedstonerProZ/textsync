package com.onrender.maskedredstonerproz.textsync.feature_online_sync.data.db

import androidx.room.*
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.dto.FileDto

/**
 * [FileDatabase] data access object
 * @author MaskedRedstonerProZ
 */
@Dao
interface FileDao {

    /**
     * File loading from database handler
     * @author MaskedRedstonerProZ
     */
    @Query("SELECT * FROM file")
    fun loadFiles(): List<FileDto>

    /**
     * File saving to database handler
     * @author MaskedRedstonerProZ
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveFile(fileDto: FileDto)

    /**
     * File deleting from database handler
     * @author MaskedRedstonerProZ
     */
    @Query("DELETE FROM file WHERE id=:fileId")
    fun deleteFile(fileId: String)

    @Query("DELETE FROM file")
    fun clearDatabase()
}
