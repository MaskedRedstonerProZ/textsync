package com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.dialogs.server_setup_dialog

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.onrender.maskedredstonerproz.textsync.R
import com.onrender.maskedredstonerproz.textsync.core.presentation.components.AutoResizedText

/**
 * The dialog for setting up the server
 * @param showDialog The state that determines if the dialog should be shown or not
 * @param serverUrl The state that holds the server url string
 * @param onShowDialogChanged The callback that handles showDialogState changes
 * @param onServerUrlTextChanged The callback that handles serverUrlState changes
 * @author MaskedRedstonerProZ
 */
@Composable
fun OnlineSyncServerSetupDialog(
    showDialog: Boolean,
    serverUrl: String,
    onShowDialogChanged: (it: Boolean, isButtonClicked: Boolean) -> Unit,
    onServerUrlTextChanged: (it: String) -> Unit
) {
    if(showDialog) {
        Dialog(onDismissRequest = { onShowDialogChanged(false, false) }) {
                Column(
                    Modifier
                        .clip(MaterialTheme.shapes.medium)
                        .background(
                            MaterialTheme.colors.background
                        )
                        .padding(15.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Row(
                        modifier = Modifier
                            .clip(MaterialTheme.shapes.medium)
                            .background(Color.Black)
                            .padding(5.dp)
                    ) {
                        OutlinedTextField(
                            value = serverUrl,
                            onValueChange = onServerUrlTextChanged,
                            textStyle = MaterialTheme.typography.body1,
                            placeholder = {
                                AutoResizedText(text = stringResource(id = R.string.enter_url))
                            },
                            singleLine = true,
                            colors = TextFieldDefaults.outlinedTextFieldColors(
                                textColor = MaterialTheme.colors.primaryVariant,
                                unfocusedBorderColor = MaterialTheme.colors.primary
                            )
                        )
                    }
                    Button(
                        onClick = {
                            onShowDialogChanged(false, true)
                        },
                        modifier = Modifier.padding(top = 15.dp)
                    ) {
                        AutoResizedText(text = stringResource(id = R.string.add_server))
                    }
            }
        }
    }
}