package com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.screens.file_list_screen

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Checkbox
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.app.ComponentActivity
import com.onrender.maskedredstonerproz.textsync.R
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.ext.component1
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.ext.component2
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.ext.openAppSettings
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.sdk30AndUp
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.sdk33AndUp
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.components.PermissionDialog
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.util.ManageExternalStoragePermissionTextProvider
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.util.PostNotificationPermissionTextProvider
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.util.ReadExternalStoragePermissionTextProvider
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.util.WriteExternalStoragePermissionTextProvider

/**
 * The local file list screen
 * @param viewModel The viewmodel for this screen
 * @param permissionHandlingViewModel The viewmodel for handling permissions
 * @param activity The root activity of all the screens
 * @author MaskedRedstonerProZ
 */
@Composable
fun FileListScreen(
    viewModel: OnlineSyncViewModel,
    permissionHandlingViewModel: PermissionHandlingViewModel,
    activity: ComponentActivity
) {
    val context = LocalContext.current

    val manageExternalStoragePermissionLauncher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (
                sdk30AndUp {
                    !Environment.isExternalStorageManager()
                } == true
            ) {
                return@rememberLauncherForActivityResult
            }

            viewModel.listTextFiles()
        }

    val permissionLauncher = rememberLauncherForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { (permissions, isGranted) ->
        permissions.forEach { permission ->
            permissionHandlingViewModel.onPermissionResult(permission, isGranted[permissions.indexOf(permission)]) {
                when(it) {
                    Manifest.permission.READ_EXTERNAL_STORAGE -> {
                        viewModel.listTextFiles()
                    }

                    Manifest.permission.WRITE_EXTERNAL_STORAGE -> {
                        viewModel.addFile(viewModel.chosenFileIndexState)
                    }

                    Manifest.permission.POST_NOTIFICATIONS -> {
                        viewModel.onPostNotificationsPermissionGranted(true)
                        viewModel.addFile(viewModel.chosenFileIndexState)
                    }
                }
            }
        }
    }

    sdk30AndUp {
        val uri = Uri.parse("package:${com.onrender.maskedredstonerproz.textsync.BuildConfig.APPLICATION_ID}")
        val intent = Intent(ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION, uri)
        SideEffect {
            if (!Environment.isExternalStorageManager()) {
                manageExternalStoragePermissionLauncher.launch(intent)
            }
        }
    } ?: SideEffect {
        permissionLauncher.launch(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE))
    }

    sdk30AndUp {
        if (Environment.isExternalStorageManager()) {
            remember {
                viewModel.listTextFiles()
                null
            }
        }
    }
        ?: if (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            remember {
                viewModel.listTextFiles()
                null
            }
        } else Unit

    Column(
        Modifier
            .padding(15.dp)
            .fillMaxSize()
    ) {

        LazyColumn(Modifier.clip(MaterialTheme.shapes.medium)) {
            if (viewModel.fileLoadingProgressState) {
                item {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        CircularProgressIndicator()
                    }
                }
            }
            items(
                items = viewModel.fileState.toList().distinct(),
                itemContent = { item ->
                    Column {
                        Row(
                            Modifier
                                .fillMaxSize()
                                .clip(MaterialTheme.shapes.medium)
                                .background(Color.Black),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Spacer(modifier = Modifier.width(15.dp))
                            Text(
                                text = item.file.name,
                                style = MaterialTheme.typography.body1,
                                color = MaterialTheme.colors.primaryVariant,
                                modifier = Modifier.weight(1F)
                            )
                            if (viewModel.fileAddingOrRemovingProgressState[viewModel.fileState.indexOf(
                                    item
                                )]
                            ) {
                                Row(
                                    modifier = Modifier.fillMaxWidth(),
                                    horizontalArrangement = Arrangement.Center
                                ) {
                                    CircularProgressIndicator(
                                        color = MaterialTheme.colors.primaryVariant
                                    )
                                }
                            }
                            Checkbox(checked = item.isSynced, onCheckedChange = {
                                if (it) {
                                    sdk33AndUp {
                                        if (context.checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                                            viewModel.onChosenFileIndexStateChanged(viewModel.fileState.indexOf(item))
                                            permissionLauncher.launch(
                                                arrayOf(
                                                    Manifest.permission.POST_NOTIFICATIONS
                                                )
                                            )
                                            return@Checkbox
                                        }

                                        viewModel.addFile(viewModel.fileState.indexOf(item))
                                    } ?: sdk30AndUp {
                                        viewModel.addFile(viewModel.fileState.indexOf(item))
                                    } ?: kotlin.run {
                                        if (context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                            permissionLauncher.launch(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                                            return@run
                                        }
                                        viewModel.addFile(viewModel.fileState.indexOf(item))
                                    }
                                }
                                if (!it) {
                                    viewModel.removeFile(viewModel.fileState.indexOf(item))
                                }
                                viewModel.fileState[viewModel.fileState.indexOf(item)] = item.copy(
                                    isSynced = it
                                )
                            })
                        }
                        if (viewModel.fileState.indexOf(item) != viewModel.fileState.lastIndex) Spacer(
                            modifier = Modifier.height(5.dp)
                        )
                    }
                }
            )
        }

        permissionHandlingViewModel.visiblePermissionDialogQueue.forEach { permission ->
            PermissionDialog(
                permissionTextProvider = when (permission) {
                    Manifest.permission.POST_NOTIFICATIONS -> PostNotificationPermissionTextProvider(stringResource(id = R.string.post_notifications))
                    Manifest.permission.READ_EXTERNAL_STORAGE -> ReadExternalStoragePermissionTextProvider(stringResource(id = R.string.read_external_storage))
                    Manifest.permission.WRITE_EXTERNAL_STORAGE -> WriteExternalStoragePermissionTextProvider(stringResource(id = R.string.write_external_storage))
                    Manifest.permission.MANAGE_EXTERNAL_STORAGE -> ManageExternalStoragePermissionTextProvider()
                    else -> return@forEach
                },
                isPermanentlyDeclined = if(permission != Manifest.permission.MANAGE_EXTERNAL_STORAGE) !activity.shouldShowRequestPermissionRationale(
                    permission
                ) else true,
                onDismiss = permissionHandlingViewModel::dismissDialog,
                onOkClick = {
                    permissionHandlingViewModel.dismissDialog()
                    permissionLauncher.launch(
                        arrayOf(permission)
                    )
                },
                onGoToAppSettingsClick = activity::openAppSettings
            )
        }
    }
}