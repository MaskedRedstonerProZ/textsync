package com.onrender.maskedredstonerproz.textsync.core.presentation.util

import androidx.compose.runtime.*

/**
 * Return a new [State] initialized with the passed in [value]
 * @param value the initial value for the [MutableState]
 * @param policy a policy to controls how changes are handled in snapshots.
 * @author MaskedRedstonerProZ
 */
fun <T> stateOf(value: T, policy: SnapshotMutationPolicy<T> = structuralEqualityPolicy()): State<T> {
    return mutableStateOf(value, policy)
}