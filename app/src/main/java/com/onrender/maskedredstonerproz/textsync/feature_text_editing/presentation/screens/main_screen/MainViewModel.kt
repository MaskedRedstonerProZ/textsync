package com.onrender.maskedredstonerproz.textsync.feature_text_editing.presentation.screens.main_screen

import android.net.Uri
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.ktor.utils.io.core.use
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.stateOf
import java.io.OutputStream
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.scheduleAtFixedRate

/**
 * The [MainViewModel] for the main screen
 * @author MaskedRedstonerProZ
 */
class MainViewModel : ViewModel() {

    /**
     * The state object representing the current state of the text field
     * @author MaskedRedstonerProZ
     */
    var textFieldState by mutableStateOf("")
        private set

    /**
     * The state object that holds the [Uri] of the opened file
     * @author MaskedRedstonerProZ
     */
    var uriState by mutableStateOf<Uri?>(null)
        private set

    /**
     * The change counter since the last time the file was saved
     * @author MaskedRedstonerProZ
     */
    var textChangeSinceLastSaveCount = 0
        private set

    /**
     * The stack state object containing the change record since the file was opened/created
     * @author MaskedRedstonerProZ
     */
    private val undoStack by stateOf(initUndoStack())

    /**
     * The stack state object containing the undone change record
     * @author MaskedRedstonerProZ
     */
    val redoStack by stateOf(Stack<String>())

    /**
     * The autosave functionality initializer
     * @author MaskedRedstonerProZ
     */
    fun initAutosave(intervalUnit: TimeUnit, interval: Long, outputStream: OutputStream) = viewModelScope.launch(Dispatchers.IO) {
        Timer().scheduleAtFixedRate(0, intervalUnit.toMillis(interval)) {
            resetTextChangeSinceLastSaveCount()
            uriState?.let {
                outputStream.use {
                    it.write(textFieldState.encodeToByteArray())
                }
            }
        }
    }

    /**
     * Undo change event handler
     * @author MaskedRedstonerProZ
     */
    fun undo() {
        val string = undoStack.pop()
        redoStack.push(string)
        textFieldState = undoStack.peek()
        if(textChangeSinceLastSaveCount > 0) textChangeSinceLastSaveCount--
    }

    /**
     * Redo change event handler
     * @author MaskedRedstonerProZ
     */
    fun redo() {
        val string = redoStack.pop()
        undoStack.push(string)
        textFieldState = string
        textChangeSinceLastSaveCount++
    }

    /**
     * Stack clearer
     * @author MaskedRedstonerProZ
     */
    fun clearStacks() {
        undoStack.clear()
        undoStack.push(initUndoStack().peek())
        if (redoStack.isNotEmpty()) redoStack.clear()
    }

    /**
     * Undo stack initializer
     * @author MaskedRedstonerProZ
     */
    private fun initUndoStack(): Stack<String> {
        val stack = Stack<String>()
        stack.push("")
        return stack
    }

    /**
     * Change counter re-setter
     * @author MaskedRedstonerProZ
     */
    fun resetTextChangeSinceLastSaveCount() {
        textChangeSinceLastSaveCount = 0
    }

    /**
     * Public-friendly uri state setter
     * @author MaskedRedstonerProZ
     */
    fun onUriStateChanged(it: Uri? = null) {
        uriState = it
    }

    /**
     * Public-friendly text field state setter
     * @author MaskedRedstonerProZ
     */
    fun onTextFieldStateChanged(it: String) {
        textFieldState = it
        undoStack.push(it)
        textChangeSinceLastSaveCount++
    }
}