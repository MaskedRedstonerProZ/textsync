package com.onrender.maskedredstonerproz.textsync.di

import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import androidx.room.Room
import androidx.work.WorkManager
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.DATABASE_NAME
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.FILE_CHANGE_ADD_WORK_NAME
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.FILE_SYNC_START_WORK_NAME
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.FILE_SYNC_WORK_NAME
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.data.db.FileDatabase
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos.DeviceRepository
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos.FileRepository
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.workmanager.*
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.screens.file_list_screen.OnlineSyncViewModel
import com.onrender.maskedredstonerproz.textsync.feature_text_editing.presentation.screens.main_screen.MainViewModel
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.serialization.kotlinx.json.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.screens.file_list_screen.PermissionHandlingViewModel

/**
 * Application module for dependency injection
 * @author MaskedRedstonerProZ
 */
val appModule = module {

    // Room database dependency definition
    single {
        Room.databaseBuilder(
            androidContext(),
            FileDatabase::class.java,
            DATABASE_NAME
        ).build()
    }

    // Room database access object dependency definition
    single {
        val database: FileDatabase = get(FileDatabase::class)
        database.dao
    }

    // MainViewModel dependency definition
    single {
        MainViewModel()
    }

    // OnlineSyncViewModel dependency definition
    single {
        OnlineSyncViewModel()
    }

    // PermissionHandlingViewModel dependency definition
    single {
        PermissionHandlingViewModel()
    }

    // SharedPreferences dependency definition
    single<SharedPreferences> {
        PreferenceManager.getDefaultSharedPreferences(androidContext())
    }

    // DeviceRepository dependency definition
    single {
        DeviceRepository.create()
    }

    // FileRepository dependency definition
    single {
        FileRepository.create()
    }

    // Http Client dependency definition
    single {
        HttpClient(Android) {
            install(Logging) {
                level = LogLevel.ALL
            }
            install(ContentNegotiation) {
                json()
            }
        }
    }

    // Work Manager dependency definition
    single {
        WorkManager.getInstance(androidContext())
    }

    // File sync start worker manager dependency definition
    single<WorkerManager>(qualifier = named(
        FILE_SYNC_START_WORK_NAME
    )) {
        FileSyncStartWorkerManager()
    }

    // File change add worker manager dependency definition
    single<WorkerManager>(qualifier = named(
        FILE_CHANGE_ADD_WORK_NAME
    )) {
        FileChangeAddWorkerManager()
    }

    // File sync worker manager dependency definition
    single<WorkerManager>(qualifier = named(
        FILE_SYNC_WORK_NAME
    )) {
        FileSyncWorkerManager()
    }

}