package com.onrender.maskedredstonerproz.textsync.core.presentation.util.ext

import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.FileListItem
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.dto.FileDto

/**
 * Map function that maps a [FileListItem] to a [FileDto] (File Data Transfer object)
 * used to store file info in the database
 * @author MaskedRedstonerProZ
 */
fun FileListItem.mapToDto(): FileDto {
    return FileDto(
        path = file.path,
        originalName = file.name,
        deviceOfOrigin = deviceOfOrigin,
        id = id
    )
}