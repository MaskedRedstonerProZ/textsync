package com.onrender.maskedredstonerproz.textsync.feature_online_sync.framework.workmanager.workers

import android.content.Context
import android.content.SharedPreferences
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.DEVICE_ID_KEY
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.requests.FileSyncRequest
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos.FileRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

/**
 * The worker for periodic file sync starting
 * @param context The context of the worker
 * @param workerParams The parameters of the worker
 * @author MaskedRedstonerProZ
 */
class FileSyncStartWorker(
    context: Context,
    workerParams: WorkerParameters
): CoroutineWorker(context, workerParams) {

    /**
     * File repository used to make the file network requests
     * @author MaskedRedstonerProZ
     */
    private val fileRepository: FileRepository by inject(FileRepository::class.java)

    /**
     * Shared preferences object used to store data like the server url
     * @author MaskedRedstonerProZ
     */
    private val sharedPreferences: SharedPreferences by inject(SharedPreferences::class.java)

    override suspend fun doWork(): Result {
        withContext(Dispatchers.IO) {
            fileRepository.startSynchronisingFiles(
                FileSyncRequest(
                    deviceId = sharedPreferences.getString(DEVICE_ID_KEY, "") ?: ""
                )
            )
        }
        return Result.success()
    }
}