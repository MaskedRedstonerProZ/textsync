package com.onrender.maskedredstonerproz.textsync.core.presentation.util.ext

operator fun <K, V> Map<K, V>.component1() = this.keys.toList()
operator fun <K, V> Map<K, V>.component2() = this.values.toList()