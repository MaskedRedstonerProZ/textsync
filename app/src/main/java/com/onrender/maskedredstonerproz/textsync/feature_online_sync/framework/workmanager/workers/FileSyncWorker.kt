package com.onrender.maskedredstonerproz.textsync.feature_online_sync.framework.workmanager.workers

import android.content.Context
import android.content.SharedPreferences
import android.os.Environment
import androidx.core.app.NotificationCompat
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.onrender.maskedredstonerproz.textsync.R
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.DEVICE_ID_KEY
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.NotificationChannelIDs.FILE_SYNC_NOTIFICATION_CHANNEL_ID
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.WorkerKeys.API_RESPONSE_KEY
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.WorkerKeys.ERROR_MESSAGE_KEY
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.FileListItem
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.requests.FileSyncRequest
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos.FileRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.koin.java.KoinJavaComponent.inject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.random.Random

/**
 * The worker for periodic file syncing
 * @param context The context of the worker
 * @param workerParams The parameters of the worker
 * @author MaskedRedstonerProZ
 */
class FileSyncWorker(
    private val context: Context,
    private val workerParams: WorkerParameters
): CoroutineWorker(context, workerParams) {

    /**
     * File repository used to make the file network requests
     * @author MaskedRedstonerProZ
     */
    private val fileRepository: FileRepository by inject(FileRepository::class.java)

    /**
     * Shared preferences object used to store data like the server url
     * @author MaskedRedstonerProZ
     */
    private val sharedPreferences: SharedPreferences by inject(SharedPreferences::class.java)

    override suspend fun doWork(): Result {
        startForeground()
        val response = fileRepository.synchroniseFiles(
            FileSyncRequest(
                sharedPreferences.getString(DEVICE_ID_KEY, "") ?: return Result.failure(
                    workDataOf(
                        ERROR_MESSAGE_KEY to "Device id is null"
                    )
                )
            )
        )
        response?.let { body ->
            return withContext(Dispatchers.IO) {
                val directory = File("${Environment.getExternalStorageDirectory()}/${context.getString(R.string.app_name)}")
                if (!directory.exists()) directory.mkdir()
                if(body.files.isEmpty()) return@withContext Result.success(
                    workDataOf(
                        API_RESPONSE_KEY to "no syncable files"
                    )
                )
                body.files.forEachIndexed { i, bytes ->
                    val file = File(directory, body.originalFileNames[i])
                    FileOutputStream(file).use { stream ->
                        try {
                            stream.write(bytes)
                        } catch (e: IOException) {
                            return@withContext Result.failure(
                                workDataOf(
                                    ERROR_MESSAGE_KEY to e.localizedMessage
                                )
                            )
                        }
                    }
                    fileRepository.addFile(
                        FileListItem(
                            file = file,
                            isSynced = true,
                            deviceOfOrigin = body.devicesOfOrigin[i],
                            id = body.fileIds[i]
                        )
                    )
                }
                return@withContext Result.success(
                    workDataOf(
                        API_RESPONSE_KEY to Json.encodeToString(response)
                    )
                )
            }
        }
        return Result.failure()
    }

    /**
     * Starts the foreground service
     * @author MaskedRedstonerProZ
     */
    private suspend fun startForeground() {
        setForeground(
            ForegroundInfo(
                Random.nextInt(),
                NotificationCompat.Builder(context, FILE_SYNC_NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(context.getString(R.string.file_sync_notification_title))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .build()
            )
        )
    }
}