package com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos

import com.onrender.maskedredstonerproz.textsync.feature_online_sync.data.api.repos.DeviceRepositoryImpl
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.Device
import io.ktor.client.*
import org.koin.java.KoinJavaComponent.inject

/**
 * [DeviceRepository] to interact with the remote sync api
 * @author MaskedRedstonerProZ
 */
interface DeviceRepository {

    /**
     * Adds the device to the remote sync api
     * @author MaskedRedstonerProZ
     */
    suspend fun addDevice(device: Device)

    /**
     * Removes the device from the remote sync api
     * @author MaskedRedstonerProZ
     */
    suspend fun removeDevice(deviceId: String)

    companion object {

        /**
         * The http client instance for the actual api interactions
         * @author MaskedRedstonerProZ
         */
        private val client: HttpClient by inject(HttpClient::class.java)

        /**
         * Device Repository generator
         * @return the generated repository
         * @author MaskedRedstonerProZ
         */
        fun create(): DeviceRepository {

            return DeviceRepositoryImpl(client)
        }
    }

}