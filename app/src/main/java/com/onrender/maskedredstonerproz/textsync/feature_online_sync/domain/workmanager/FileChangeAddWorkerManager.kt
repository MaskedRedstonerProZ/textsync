package com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.workmanager

import androidx.work.*
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.FILE_CHANGE_ADD_WORK_NAME
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.framework.workmanager.workers.FileChangeAddWorker
import org.koin.java.KoinJavaComponent.inject
import java.util.concurrent.TimeUnit

/**
 * The manager for the [FileChangeAddWorker]
 * @author MaskedRedstonerProZ
 */
class FileChangeAddWorkerManager: WorkerManager {

    override val workManager: WorkManager by inject(WorkManager::class.java)

    /**
     * The periodic file change add work request
     * @author MaskedRedstonerProZ
     */
    private lateinit var fileChangeAddRequest: PeriodicWorkRequest

    override fun initializeWorker() {
        fileChangeAddRequest = PeriodicWorkRequestBuilder<FileChangeAddWorker>(30, TimeUnit.MINUTES)
            .setConstraints(
                Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
            )
            .build()
    }

    override fun enqueueWork() {
        workManager.enqueueUniquePeriodicWork(
            FILE_CHANGE_ADD_WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            fileChangeAddRequest
        )
    }

    override fun cancelWork() {
        workManager.cancelUniqueWork(FILE_CHANGE_ADD_WORK_NAME)
    }
}