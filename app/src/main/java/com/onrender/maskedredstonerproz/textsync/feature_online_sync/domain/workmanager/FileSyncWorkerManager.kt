package com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.workmanager

import androidx.work.*
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.FILE_SYNC_WORK_NAME
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.framework.workmanager.workers.FileSyncWorker
import org.koin.java.KoinJavaComponent.inject
import java.util.concurrent.TimeUnit

/**
 * The manager for the [FileSyncWorker]
 * @author MaskedRedstonerProZ
 */
class FileSyncWorkerManager: WorkerManager {

    override val workManager: WorkManager by inject(WorkManager::class.java)

    /**
     * The periodic file sync work request
     * @author MaskedRedstonerProZ
     */
    lateinit var fileSyncRequest: PeriodicWorkRequest

    override fun initializeWorker() {
        fileSyncRequest = PeriodicWorkRequestBuilder<FileSyncWorker>(30, TimeUnit.MINUTES)
            .setConstraints(
                Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .setRequiresStorageNotLow(true)
                    .build()
            )
            .build()
    }

    override fun enqueueWork() {
        workManager.enqueueUniquePeriodicWork(
            FILE_SYNC_WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            fileSyncRequest
        )
    }

    override fun cancelWork() {
        workManager.cancelUniqueWork(FILE_SYNC_WORK_NAME)
    }
}