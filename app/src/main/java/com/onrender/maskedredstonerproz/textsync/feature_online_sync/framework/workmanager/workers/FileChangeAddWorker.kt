package com.onrender.maskedredstonerproz.textsync.feature_online_sync.framework.workmanager.workers

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.onrender.maskedredstonerproz.textsync.R
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.NotificationChannelIDs.FILE_CHANGE_ADD_NOTIFICATION_CHANNEL_ID
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.WorkerKeys.ERROR_MESSAGE_KEY
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.FileListItem
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.requests.AddFileRequest
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos.FileRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject
import kotlin.random.Random

/**
 * The worker for periodic file change adding
 * @param context The context of the worker
 * @param workerParams The parameters of the worker
 * @author MaskedRedstonerProZ
 */
class FileChangeAddWorker(
    private val context: Context,
    workerParams: WorkerParameters
): CoroutineWorker(context, workerParams) {

    /**
     * File repository used to make the file network requests
     * @author MaskedRedstonerProZ
     */
    private val fileRepository: FileRepository by inject(FileRepository::class.java)

    override suspend fun doWork(): Result {
        startForeground()
        withContext(Dispatchers.IO) {
            val responses = loadFilesFromLocalDatabase().map { fileListItem: FileListItem ->
                fileRepository.addFile(
                    AddFileRequest(
                        originalName = fileListItem.file.name,
                        deviceOfOrigin = fileListItem.deviceOfOrigin,
                        fileId = fileListItem.id
                    ), fileListItem.file
                )
            }
            responses.forEach {
                if(it == null) {
                    return@withContext Result.failure(
                        workDataOf(
                            ERROR_MESSAGE_KEY to "Nepoznata greska"
                        )
                    )
                }
            }
        }
        return Result.success()
    }

    /**
     * Starts the foreground service
     * @author MaskedRedstonerProZ
     */
    private suspend fun startForeground() {
        setForeground(
            ForegroundInfo(
                Random.nextInt(),
                NotificationCompat.Builder(context, FILE_CHANGE_ADD_NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(context.getString(R.string.file_change_add_notification_title))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .build()
            )
        )
    }

    /**
     * Fetches the file info from the local database
     * @author MaskedRedstonerProZ
     */
    private suspend fun loadFilesFromLocalDatabase(): List<FileListItem> {
        return fileRepository.loadFiles()
    }
}