package com.onrender.maskedredstonerproz.textsync.core.presentation.util

import androidx.annotation.StringRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource

/**
 * Handler for managing text which may come from different sources
 * in the viewModel
 * @author MaskedRedstonerProZ
 */
sealed class UiText {

    /**
     * Text which comes from a dynamic source(eg. the api, a local database, etc.)
     * @param value The actual text value
     * @author MaskedRedstonerProZ
     */
    data class DynamicString(val value: String): UiText()

    /**
     * Text which comes from a string resource
     * @param resId The id of the string resource
     * @param args The set of potential arguments to complete the string
     * @author MaskedRedstonerProZ
     */
    class StringResource(

        @StringRes
        val resId: Int,
        vararg val args: Any
    ): UiText()

    /**
     * Extract the string from the handler
     * @author MaskedRedstonerProZ
     */
    @Composable
    fun asString(): String {
        return when(this) {
            is DynamicString -> value
            is StringResource -> stringResource(id = resId, formatArgs = args)
        }
    }
}