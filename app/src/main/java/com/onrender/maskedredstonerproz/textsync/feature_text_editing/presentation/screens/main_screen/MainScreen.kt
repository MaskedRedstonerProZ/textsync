package com.onrender.maskedredstonerproz.textsync.feature_text_editing.presentation.screens.main_screen

import android.content.Intent.ACTION_VIEW
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.Button
import androidx.compose.material.DrawerState
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Create
import androidx.compose.material.icons.outlined.Redo
import androidx.compose.material.icons.outlined.Undo
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.app.ComponentActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.onrender.maskedredstonerproz.textsync.R
import com.onrender.maskedredstonerproz.textsync.core.presentation.components.AutoResizedText
import com.onrender.maskedredstonerproz.textsync.feature_text_editing.presentation.components.StandardAppBar
import java.util.concurrent.TimeUnit

/**
 * Screen that contains the main functionality of this app
 * @param scope The coroutine scope for this screen
 * @param drawerState The drawer state for the navigation drawer
 * @param viewModel The viewModel for this screen
 * @param activity The root activity of all the screens
 * @author MaskedRedstonerProZ
 */
@Composable
fun MainScreen(
    scope: CoroutineScope,
    drawerState: DrawerState,
    viewModel: MainViewModel,
    activity: ComponentActivity
) {
    val context = LocalContext.current


    val intent = activity.intent
    val action = intent.action
    val type = intent.type

    if (action == ACTION_VIEW && type == "text/plain") {
        intent.data?.let {
            viewModel.onUriStateChanged(it)
            context.contentResolver.openInputStream(viewModel.uriState ?: return@let)
                .use { stream ->
                    viewModel.onTextFieldStateChanged(
                        stream?.readBytes()?.decodeToString() ?: "error"
                    )
                    viewModel.resetTextChangeSinceLastSaveCount()
                    viewModel.clearStacks()
                }
        }
    }

    Column {
        StandardAppBar(
            modifier = Modifier.fillMaxWidth(),
            scope = scope,
            drawerState = drawerState
        ) {

            IconButton(
                onClick = {
                    viewModel.undo()
                },
                enabled = viewModel.textFieldState.isNotBlank()
            ) {
                Icon(Icons.Outlined.Undo, "undo")
            }

            IconButton(
                onClick = {
                    viewModel.redo()
                },
                enabled = viewModel.redoStack.isNotEmpty()
            ) {
                Icon(Icons.Outlined.Redo, "redo")
            }

            IconButton(
                onClick = {
                    if (viewModel.textChangeSinceLastSaveCount > 0) {
                        Toast.makeText(
                            context,
                            context.getText(R.string.file_changed_warning_alt),
                            Toast.LENGTH_LONG
                        ).show()
                        return@IconButton
                    }

                    viewModel.apply {
                        onUriStateChanged()
                        onTextFieldStateChanged("")
                        resetTextChangeSinceLastSaveCount()
                        clearStacks()
                    }
                }
            ) {
                Icon(Icons.Outlined.Create, "create")
            }
        }
        Column {
            TextField(
                value = viewModel.textFieldState,
                onValueChange = {
                    if (viewModel.uriState != null && viewModel.textChangeSinceLastSaveCount == 0) {
                        viewModel.initAutosave(
                            TimeUnit.MINUTES,
                            1,
                            context.contentResolver.openOutputStream(viewModel.uriState!!)!!
                        )
                    }
                    viewModel.onTextFieldStateChanged(it)
                },
                singleLine = false,
                colors = TextFieldDefaults.textFieldColors(
                    unfocusedIndicatorColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent,
                    errorIndicatorColor = Color.Transparent,
                    backgroundColor = Color.Transparent
                ),
                shape = MaterialTheme.shapes.small.copy(CornerSize(0.dp)),
                modifier = Modifier.weight(1f)
            )
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(15.dp),
                horizontalArrangement = Arrangement.SpaceAround,
                verticalAlignment = Alignment.CenterVertically
            ) {
                val openFileLauncher =
                    rememberLauncherForActivityResult(contract = ActivityResultContracts.OpenDocument()) {
                        viewModel.onUriStateChanged(it)
                        context.contentResolver.openInputStream(
                            viewModel.uriState ?: return@rememberLauncherForActivityResult
                        ).use { stream ->
                            viewModel.onTextFieldStateChanged(
                                stream?.readBytes()?.decodeToString() ?: "error"
                            )
                            viewModel.resetTextChangeSinceLastSaveCount()
                            viewModel.clearStacks()
                        }
                    }
                Button(
                    modifier = Modifier.weight(1f),
                    onClick = {
                        openFileLauncher.launch(arrayOf("text/plain"))
                    }
                ) {
                    AutoResizedText(
                        text = stringResource(id = R.string.open)
                    )
                }
                Spacer(modifier = Modifier.width(15.dp))
                Button(
                    modifier = Modifier.weight(1f),
                    onClick = {

                        if (viewModel.textFieldState.isEmpty()) {
                            viewModel.onUriStateChanged()
                            viewModel.clearStacks()
                            return@Button
                        }

                        if (viewModel.textChangeSinceLastSaveCount > 0) {
                            Toast.makeText(
                                context,
                                context.getText(R.string.file_changed_warning),
                                Toast.LENGTH_LONG
                            ).show()
                            return@Button
                        }

                        viewModel.onUriStateChanged()
                        viewModel.clearStacks()
                        viewModel.onTextFieldStateChanged("")
                    }
                ) {
                    AutoResizedText(
                        text = stringResource(id = R.string.close)
                    )
                }
                val saveFileLauncher = rememberLauncherForActivityResult(
                    contract = ActivityResultContracts.CreateDocument("text/plain")
                ) {
                    val file = it?.let { it1 -> context.contentResolver.openOutputStream(it1) }
                    val text = viewModel.textFieldState
                    viewModel.onUriStateChanged(it)
                    scope.launch {
                        withContext(Dispatchers.IO) {
                            file?.write(text.encodeToByteArray())
                            file?.close()
                        }
                    }
                }
                Spacer(modifier = Modifier.width(15.dp))
                Button(
                    modifier = Modifier.weight(1f),
                    onClick = {
                        if(viewModel.textFieldState.isEmpty()) return@Button

                        viewModel.resetTextChangeSinceLastSaveCount()
                        viewModel.uriState?.let {
                            val file = context.contentResolver.openOutputStream(it)
                            val text = viewModel.textFieldState
                            scope.launch {
                                withContext(Dispatchers.IO) {
                                    file?.write(text.encodeToByteArray())
                                    file?.close()
                                }
                            }
                            return@Button
                        }

                        saveFileLauncher.launch("")
                    }
                ) {
                    AutoResizedText(
                        text = stringResource(id = R.string.save)
                    )
                }
            }
        }
    }
}