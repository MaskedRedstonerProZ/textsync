package com.onrender.maskedredstonerproz.textsync.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.Composable

/**
 * The colour pallete for the project
 * @author MaskedRedstonerProZ
 */
private val DarkColorPallete = darkColors(
    background = darkGray,
    onBackground = black,
    primary = red,
    primaryVariant = white,
    onPrimary = blue,
)

/**
 * The theme of the project
 * @author MaskedRedstonerProZ
 */
@Composable
fun TextPadTheme(content: @Composable () -> Unit) {
    MaterialTheme(
        colors = DarkColorPallete,
        typography = Typography,
        shapes = Shapes,
        content = content
    )

}