package com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.workmanager

import androidx.work.WorkManager

/**
 * The manager for each worker
 * @author MaskedRedstonerProZ
 */
interface WorkerManager {

    /**
     * The workManager instance used to interact with the workers
     * @author MaskedRedstonerProZ
     */
    val workManager: WorkManager

    /**
     * The worker initializer
     * @author MaskedRedstonerProZ
     */
    fun initializeWorker()

    /**
     * The work enqueuer
     * @author MaskedRedstonerProZ
     */
    fun enqueueWork()

    /**
     * The work canceller
     * @author MaskedRedstonerProZ
     */
    fun cancelWork()

}