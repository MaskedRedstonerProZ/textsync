package com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.workmanager

import androidx.work.*
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.FILE_SYNC_START_WORK_NAME
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.framework.workmanager.workers.FileSyncStartWorker
import org.koin.java.KoinJavaComponent.inject
import java.util.concurrent.TimeUnit

/**
 * The manager for the [FileSyncStartWorker]
 * @author MaskedRedstonerProZ
 */
class FileSyncStartWorkerManager: WorkerManager {

    override val workManager: WorkManager by inject(WorkManager::class.java)

    /**
     * The periodic file sync start work request
     * @author MaskedRedstonerProZ
     */
    private lateinit var fileSyncStartRequest: PeriodicWorkRequest

    override fun initializeWorker() {
        fileSyncStartRequest = PeriodicWorkRequestBuilder<FileSyncStartWorker>(30, TimeUnit.MINUTES)
            .setConstraints(
                Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
            )
            .build()
    }

    override fun enqueueWork() {
        workManager.enqueueUniquePeriodicWork(
            FILE_SYNC_START_WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            fileSyncStartRequest
        )
    }

    override fun cancelWork() {
        workManager.cancelUniqueWork(FILE_SYNC_START_WORK_NAME)
    }

}