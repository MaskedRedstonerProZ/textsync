package com.onrender.maskedredstonerproz.textsync.feature_online_sync.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.dto.FileDto

/**
 * The [FileDatabase] class that describes the database
 * @author MaskedRedstonerProZ
 */
@Database(
    entities = [FileDto::class],
    version = 1,
    exportSchema = false
)
abstract class FileDatabase: RoomDatabase() {

    /**
     * [FileDatabase] data access object instance
     * @author MaskedRedstonerProZ
     */
    abstract val dao: FileDao
}
