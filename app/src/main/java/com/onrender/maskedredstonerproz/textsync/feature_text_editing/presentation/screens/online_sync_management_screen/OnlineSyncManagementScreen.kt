package com.onrender.maskedredstonerproz.textsync.feature_text_editing.presentation.screens.online_sync_management_screen

import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.DrawerState
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.RemoveCircleOutline
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import kotlinx.coroutines.CoroutineScope
import com.onrender.maskedredstonerproz.textsync.R
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.components.ConfirmationDialog
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.screens.file_list_screen.FileListScreen
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.screens.file_list_screen.OnlineSyncViewModel
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.screens.file_list_screen.PermissionHandlingViewModel
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.screens.server_setup_instructions_screen.ServerSetupInstructionsScreen
import com.onrender.maskedredstonerproz.textsync.feature_text_editing.presentation.components.StandardAppBar

/**
 * The screen that contains the online sync management functionality
 * @param scope The coroutine scope for this screen
 * @param drawerState The drawer state for the navigation drawer
 * @param viewModel The viewmodel for this screen
 * @param permissionHandlingViewModel The viewmodel for handling permissions
 * @author MaskedRedstonerProZ
 */
@Composable
fun OnlineSyncManagementScreen(
    scope: CoroutineScope,
    drawerState: DrawerState,
    viewModel: OnlineSyncViewModel,
    permissionHandlingViewModel: PermissionHandlingViewModel,
    activity: ComponentActivity
) {
    val context = LocalContext.current
    Column {
        StandardAppBar(modifier = Modifier.fillMaxWidth(), scope = scope, drawerState = drawerState) {
            if(viewModel.serverUrlState.isNotBlank()) {
                IconButton(
                    onClick = {
                        viewModel.onShouldShowConfirmationDialogStateChanged(true)
                    }
                ) {
                    Icon(Icons.Outlined.RemoveCircleOutline, contentDescription = "remove server")
                }
            }
        }

        if (viewModel.serverUrlState.isBlank()) {
            ServerSetupInstructionsScreen(viewModel)
        } else FileListScreen(viewModel, permissionHandlingViewModel, activity)

        if(viewModel.shouldShowConfirmationDialogState) {
            ConfirmationDialog(
                text = stringResource(id = R.string.question),
                onDismiss = {
                    viewModel.onShouldShowConfirmationDialogStateChanged(false)
                },
                onOkClick = {
                    viewModel.onShouldShowConfirmationDialogStateChanged(false)
                    viewModel.removeDevice()
                    Toast.makeText(
                        context,
                        context.getString(R.string.server_url_removed_notice),
                        Toast.LENGTH_LONG
                    ).show()
                    viewModel.onServerUrlStateChanged("")
                    viewModel.removeServerUrl()
                }
            )
        }
    }
}