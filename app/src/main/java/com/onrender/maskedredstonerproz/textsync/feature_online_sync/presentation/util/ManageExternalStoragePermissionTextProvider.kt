package com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.util

import com.onrender.maskedredstonerproz.textsync.R
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.UiText
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.components.PermissionTextProvider

class ManageExternalStoragePermissionTextProvider: PermissionTextProvider("") {
    override fun getDescription(isPermanentlyDeclined: Boolean) = UiText.StringResource(R.string.manage_external_storage_permission_desc)
}