package com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos

import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.responses.FileSyncResponse
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.data.db.FileDao
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.data.db.FileDatabase
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.data.api.repos.FileRepositoryImpl
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.FileListItem
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.requests.AddFileRequest
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.requests.FileSyncRequest
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.responses.AddFileResponse
import io.ktor.client.*
import org.koin.java.KoinJavaComponent.inject
import java.io.File

/**
 * [FileRepository] to interact with the remote sync api
 * @author MaskedRedstonerProZ
 */
interface FileRepository {

    /**
     * Adds the file to the remote sync api
     * @author MaskedRedstonerProZ
     */
    suspend fun addFile(fileRequest: AddFileRequest, file: File): AddFileResponse?

    /**
     * Adds the device to the local database
     * @author MaskedRedstonerProZ
     */
    suspend fun addFile(fileListItem: FileListItem)

    /**
     * Loads the files from the local database
     * @author MaskedRedstonerProZ
     */
    suspend fun loadFiles(): List<FileListItem>

    /**
     * Removes the file from the remote sync api
     * @author MaskedRedstonerProZ
     */
    suspend fun removeFile(fileId: String)

    /**
     * Removes the file from the local database
     * @author MaskedRedstonerProZ
     */
    suspend fun removeFile(fileListItem: FileListItem)

    /**
     * Clears the local database
     * @author MaskedRedstonerProZ
     */
    suspend fun clearLocalDatabase()

    /**
     * Starts synchronising files to the remote sync api
     * @author MaskedRedstonerProZ
     */
    suspend fun startSynchronisingFiles(fileSyncRequest: FileSyncRequest)

    /**
     * Stops synchronising files to the remote sync api
     * @author MaskedRedstonerProZ
     */
    suspend fun stopSynchronisingFile(fileSyncRequest: FileSyncRequest)

    /**
     * Synchronises files to the remote sync api
     * @author MaskedRedstonerProZ
     */
    suspend fun synchroniseFiles(fileSyncRequest: FileSyncRequest): FileSyncResponse?

    companion object {

        /**
         * The http client instance for the actual api interactions
         * @author MaskedRedstonerProZ
         */
        private val client: HttpClient by inject(HttpClient::class.java)

        /**
         * [FileDatabase] data access object instance for interacting with the local database
         * @author MaskedRedstonerProZ
         */
        private val dao: FileDao by inject(FileDao::class.java)

        /**
         * File Repository generator
         * @return the generated repository
         * @author MaskedRedstonerProZ
         */
        fun create(): FileRepository {

            return FileRepositoryImpl(client, dao)
        }
    }
}