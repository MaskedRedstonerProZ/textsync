package com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.util

import com.onrender.maskedredstonerproz.textsync.R
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.UiText
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.components.PermissionTextProvider

class WriteExternalStoragePermissionTextProvider(override val permissionName: String): PermissionTextProvider(permissionName) {
    override fun getDescription(isPermanentlyDeclined: Boolean) = if(isPermanentlyDeclined) UiText.StringResource(R.string.permission_rationale, permissionName) else UiText.StringResource(R.string.write_external_storage_permission_desc)
}