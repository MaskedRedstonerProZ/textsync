package com.onrender.maskedredstonerproz.textsync.core.presentation.util

import android.os.Build

/**
 * Sdk version verifier helper lambda
 * @author MaskedRedstonerProZ
 */
inline fun <T> sdk33AndUp(onSdk33: () -> T): T? = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) { onSdk33() } else null

/**
 * Sdk version verifier helper lambda
 * @author MaskedRedstonerProZ
 */
inline fun <T> sdk30AndUp(onSdk30: () -> T): T? = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) { onSdk30() } else null