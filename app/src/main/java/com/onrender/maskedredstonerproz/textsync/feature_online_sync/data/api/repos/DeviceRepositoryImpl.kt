package com.onrender.maskedredstonerproz.textsync.feature_online_sync.data.api.repos

import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.DEBUG_TAG
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.DEVICE_ID_KEY
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.HttpRoutes.ROUTE_DEVICE_ADD
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.HttpRoutes.ROUTE_DEVICE_REMOVE
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.SERVER_URL_KEY
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.Device
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.requests.DeviceAddOrRemoveRequest
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos.DeviceRepository
import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import org.koin.java.KoinJavaComponent.inject

/**
 * [DeviceRepository] implementation to interact with the remote sync api
 * @param client The http client for the actual api interactions
 * @author MaskedRedstonerProZ
 */
class DeviceRepositoryImpl(
    private val client: HttpClient
): DeviceRepository {

    /**
     * Shared preferences object used to store data like the server url
     * @author MaskedRedstonerProZ
     */
    private val sharedPreferences: SharedPreferences by inject(SharedPreferences::class.java)

    /**
     * Base url for the api
     * @author MaskedRedstonerProZ
     */
    private val BASE_URL = sharedPreferences.getString(SERVER_URL_KEY, "error") ?: "error"

    override suspend fun addDevice(device: Device) {
        sharedPreferences.edit {
            putString(DEVICE_ID_KEY, device.deviceId)
        }
        try {
            client.post {
                url("$BASE_URL$ROUTE_DEVICE_ADD")
                contentType(ContentType.Application.Json)
                setBody(
                    DeviceAddOrRemoveRequest(
                        device.deviceId
                    )
                )
            }
        } catch(e: RedirectResponseException) {
            Log.d("$DEBUG_TAG: Server Exception1","Error: ${e.response.status.description}")
        } catch(e: ClientRequestException) {
            Log.d("$DEBUG_TAG: Server Exception2","Error: ${e.response.status.description}")
        } catch(e: ServerResponseException) {
            Log.d("$DEBUG_TAG: Server Exception3","Error: ${e.response.status.description}")
        } catch(e: Exception) {
            Log.d("$DEBUG_TAG: Server Exception4","Error: ${e.stackTrace.asList()}")
        }
    }

    override suspend fun removeDevice(deviceId: String) {
        sharedPreferences.edit {
            remove(DEVICE_ID_KEY)
        }
        try {
            client.delete {
                url("$BASE_URL$ROUTE_DEVICE_REMOVE")
                contentType(ContentType.Application.Json)
                setBody(
                    DeviceAddOrRemoveRequest(
                        deviceId
                    )
                )
            }
        } catch(e: RedirectResponseException) {
            Log.d("$DEBUG_TAG: Server Exception1: ","Error: ${e.response.status.description}")
        } catch(e: ClientRequestException) {
            Log.d("$DEBUG_TAG: Server Exception2: ","Error: ${e.response.status.description}")
        } catch(e: ServerResponseException) {
            Log.d("$DEBUG_TAG: Server Exception3: ","Error: ${e.response.status.description}")
        } catch(e: Exception) {
            Log.d("$DEBUG_TAG: Server Exception4: ","Error: ${e.stackTrace.asList()}")
        }
    }
}