package com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.screens.file_list_screen

import android.content.Context
import android.content.SharedPreferences
import android.os.Environment
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.DEVICE_ID_KEY
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.FILE_CHANGE_ADD_WORK_NAME
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.FILE_SYNC_START_WORK_NAME
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.FILE_SYNC_WORK_NAME
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.SERVER_URL_KEY
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.sdk33AndUp
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos.DeviceRepository
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.api.repos.FileRepository
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.Device
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.FileListItem
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.requests.AddFileRequest
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.requests.FileSyncRequest
import com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.workmanager.WorkerManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.koin.core.qualifier.named
import org.koin.java.KoinJavaComponent.inject
import java.util.*

/**
 * The [OnlineSyncViewModel] for the online sync screens
 * @author MaskedRedstonerProZ
 */
class OnlineSyncViewModel : ViewModel() {

    /**
     * Application context
     * @author MaskedRedstonerProZ
     */
    val context: Context by inject(Context::class.java)

    /**
     * Device repository used to make the device network requests
     * @author MaskedRedstonerProZ
     */
    private val deviceRepository: DeviceRepository by inject(DeviceRepository::class.java)

    /**
     * File repository used to make the file network requests
     * @author MaskedRedstonerProZ
     */
    private val fileRepository: FileRepository by inject(FileRepository::class.java)

    /**
     * Shared preferences object used to store data like the server url
     * @author MaskedRedstonerProZ
     */
    private val sharedPreferences: SharedPreferences by inject(SharedPreferences::class.java)

    /**
     * Worker manager object used to periodically start file syncing for every new file
     * @author MaskedRedstonerProZ
     */
    private val fileSyncStartWorkerManager: WorkerManager by inject(qualifier = named(FILE_SYNC_START_WORK_NAME), clazz = WorkerManager::class.java)

    /**
     * Worker manager object used to periodically add local changes made to files to the remote sync api
     * @author MaskedRedstonerProZ
     */
    private val fileChangeAddWorkerManager: WorkerManager by inject(qualifier = named(FILE_CHANGE_ADD_WORK_NAME), clazz = WorkerManager::class.java)

    /**
     * Worker manager object used to periodically download files from the remote sync api
     * @author MaskedRedstonerProZ
     */
    private val fileSyncWorkerManager: WorkerManager by inject(qualifier = named(FILE_SYNC_WORK_NAME), clazz = WorkerManager::class.java)

    /**
     * The state object that holds the file list state
     * @author MaskedRedstonerProZ
     */
    val fileState = mutableStateListOf<FileListItem>()

    /**
     * The state object that holds the dialog visibility state
     * @author MaskedRedstonerProZ
     */
    var dialogState by mutableStateOf(false)
        private set

    /**
     * The state object that holds the server url text state
     * @author MaskedRedstonerProZ
     */
    var serverUrlTextState by mutableStateOf("")
        private set

    /**
     * The state object that holds the server url state
     * @author MaskedRedstonerProZ
     */
    var serverUrlState by mutableStateOf(sharedPreferences.getString(SERVER_URL_KEY, "")?: "")
        private set

    /**
     * The state object that holds the state that determines
     * if the device adding progress indicator should be visible
     * @author MaskedRedstonerProZ
     */
    var deviceAddingProgressState by mutableStateOf(false)
        private set

    /**
     * The state object that holds the state that determines
     * if the file adding/removing progress indicator(s) should be visible
     * @author MaskedRedstonerProZ
     */
    var fileAddingOrRemovingProgressState = mutableStateListOf<Boolean>()
        private set

    /**
     * The state object that holds the state that determines
     * if the file loading progress from the local database indicator should be visible
     * @author MaskedRedstonerProZ
     */
    var fileLoadingProgressState by mutableStateOf(false)
        private set

    /**
     * The state object that determines
     * if the permission to post notifications is granted (required only on api 33+)
     * @author MaskedRedstonerProZ
     */
    private var postNotificationsPermissionGrantedState by mutableStateOf(false)

    /**
     * The state object that contains the index of the chosen file
     * @author MaskedRedstonerProZ
     */
    var chosenFileIndexState by mutableStateOf(0)
        private set

    /**
     * The state object that contains the info if the confirmation dialog should be shown or not
     * @author MaskedRedstonerProZ
     */
    var shouldShowConfirmationDialogState by mutableStateOf(false)
        private set

    /**
     * Public-friendly should show confirmation dialog state setter
     * @param value The new state value to set
     * @author MaskedRedstonerProZ
     */
    fun onShouldShowConfirmationDialogStateChanged(value: Boolean) {
        shouldShowConfirmationDialogState = value
    }

    /**
     * Public-friendly chosen file index state setter
     * @param value The new state value to set
     * @author MaskedRedstonerProZ
     */
    fun onChosenFileIndexStateChanged(value: Int) {
        chosenFileIndexState = value
    }

    /**
     * Public-friendly post notification permission granted status state setter
     * @param value The new state value to set
     * @author MaskedRedstonerProZ
     */
    fun onPostNotificationsPermissionGranted(value: Boolean) {
        postNotificationsPermissionGrantedState = value
    }

    /**
     * Public-friendly local database file loading progress state setter
     * @param value The new state value to set
     * @author MaskedRedstonerProZ
     */
    private fun onFileLoadingProgressStateChanged(value: Boolean) {
        fileLoadingProgressState = value
    }

    /**
     * Public-friendly local database file adding/removing progress state setter
     * @param changeIndex The index at which the [value] should be set
     * @param value The new state value to set
     * @author MaskedRedstonerProZ
     */
    private fun onFileAddingOrRemovingProgressStateChanged(changeIndex: Int, value: Boolean) {
         fileAddingOrRemovingProgressState[changeIndex] = value
    }

    /**
     * Public-friendly device adding progress state setter
     * @param value The new state value to set
     * @author MaskedRedstonerProZ
     */
    private fun onDeviceAddingProgressStateChanged(value: Boolean) {
        deviceAddingProgressState = value
    }

    /**
     * Public-friendly server url state setter
     * @param value The new state value to set
     * @author MaskedRedstonerProZ
     */
    fun onServerUrlStateChanged(value: String) {
        serverUrlState = value
    }

    /**
     * Public-friendly server url text state setter
     * @param value The new state value to set
     * @author MaskedRedstonerProZ
     */
    fun onServerUrlTextStateChanged(value: String) {
        serverUrlTextState = value
    }

    /**
     * Public-friendly dialog visibility state setter
     * @param value The new state value to set
     * @author MaskedRedstonerProZ
     */
    fun onDialogStateChanged(value: Boolean) {
        dialogState = value
    }

    /**
     * Initialises the periodic file change adding work manager job
     * @author MaskedRedstonerProZ
     */
    private fun fileChangeAdd() {
        fileChangeAddWorkerManager.apply {
            initializeWorker()
            enqueueWork()
        }
    }

    /**
     * Initialises the periodic file sync starting for every new file work manager job
     * @author MaskedRedstonerProZ
     */
    private fun startFileSync() {
        fileSyncStartWorkerManager.apply {
            initializeWorker()
            enqueueWork()
        }
    }

    /**
     * Initialises the file syncing work manager job
     * @author MaskedRedstonerProZ
     */
    private fun syncFiles() {
        fileSyncWorkerManager.apply {
            initializeWorker()
            enqueueWork()
        }
    }

    /**
     * Fetches a list of all text files on the user's device(in the documents and downloads folder(directly, no subfolders supported))
     * @author MaskedRedstonerProZ
     */
    fun listTextFiles() {
        viewModelScope.launch(Dispatchers.IO) {
            Environment.getExternalStorageDirectory().walkTopDown().forEach { file ->
                if(file.isFile && file.extension == "txt") {
                    fileState.add(
                        FileListItem(file, id = "", deviceOfOrigin = sharedPreferences.getString(
                            DEVICE_ID_KEY, "")?: "")
                    )
                    fileAddingOrRemovingProgressState.add(false)
                }
            }
        }.invokeOnCompletion {
            loadFilesFromLocalDatabase()
        }
    }

    /**
     * Adds the chosen file to the local database as well as the remote sync api
     * @param fileIndex The index of the chosen file to add
     * @author MaskedRedstonerProZ
     */
    fun addFile(fileIndex: Int) {
        val file = fileState[fileIndex].file
        onFileAddingOrRemovingProgressStateChanged(fileIndex, true)
        viewModelScope.launch(Dispatchers.IO) {
            val response = fileRepository.addFile(
                AddFileRequest(
                    file.name,
                    sharedPreferences.getString(DEVICE_ID_KEY, "") ?: "",
                ), file
            )

            fileState[fileIndex] = fileState[fileIndex].copy(
                id = response?.fileId.toString()
            )

            fileRepository.addFile(fileState[fileIndex])
        }.invokeOnCompletion {
            onFileAddingOrRemovingProgressStateChanged(fileIndex, false)
            if(it == null) {
                sdk33AndUp {
                    if (fileState.count { item -> item.isSynced } == 1 && postNotificationsPermissionGrantedState) {
                        fileChangeAdd()
                        syncFiles()
                    }
                } ?: if (fileState.count { item -> item.isSynced } == 1) {
                    fileChangeAdd()
                    syncFiles()
                } else Unit
            }
        }
    }

    /**
     * Removes the file from the local database and remote sync api,
     * if the file is not from this device, stops syncing the changes to it without removing it from the api
     * @param fileIndex The index of the chosen file to remove
     * @author MaskedRedstonerProZ
     */
    fun removeFile(fileIndex: Int) {
        onFileAddingOrRemovingProgressStateChanged(fileIndex, true)
        val file = fileState[fileIndex]
        val deviceId = sharedPreferences.getString(DEVICE_ID_KEY, "")?: ""
        
        if(file.deviceOfOrigin != deviceId) {
            viewModelScope.launch(Dispatchers.IO) {
                fileRepository.stopSynchronisingFile(
                    FileSyncRequest(
                        deviceId = deviceId,
                        fileId = file.id
                    )
                )

                fileRepository.removeFile(file.id)
            }.invokeOnCompletion {
                onFileAddingOrRemovingProgressStateChanged(fileIndex, false)
                if(it == null && fileState.find { fileListItem -> fileListItem.isSynced } == null) {
                    fileSyncStartWorkerManager.cancelWork()
                    fileSyncWorkerManager.cancelWork()
                }
            }
            return
        }

        viewModelScope.launch(Dispatchers.IO) {
            fileRepository.removeFile(file)
            fileRepository.removeFile(file.id)
        }.invokeOnCompletion {
            onFileAddingOrRemovingProgressStateChanged(fileIndex, false)
        }
    }
    
    /**
     * Fetches the file info from the local database
     * @author MaskedRedstonerProZ
     */
    private fun loadFilesFromLocalDatabase() {
        onFileLoadingProgressStateChanged(true)
        viewModelScope.launch(Dispatchers.IO) {
            val files = fileRepository.loadFiles()
            if(files.isEmpty()) {
                onFileLoadingProgressStateChanged(false)
                cancel()
            }
            files.forEach { item ->
                val index = fileState.indexOf(fileState.find { it.file == item.file })
                fileState[index] = fileState[index].copy(
                    isSynced = item.isSynced,
                    deviceOfOrigin = item.deviceOfOrigin,
                    id = item.id
                )
            }
        }.invokeOnCompletion {
            onFileLoadingProgressStateChanged(false)
        }
    }

    /**
     * Save server url event handler
     * @author MaskedRedstonerProZ
     */
    fun saveServerUrl() {
        sharedPreferences.edit().putString(SERVER_URL_KEY, serverUrlTextState).apply()
    }

    /**
     * Remove server url event handler
     * @author MaskedRedstonerProZ
     */
    fun removeServerUrl() {
        sharedPreferences.edit().remove(SERVER_URL_KEY).apply()
    }

    /**
     * The add device event handler
     * @author MaskedRedstonerProZ
     */
    fun addDevice() {
        viewModelScope.launch(Dispatchers.IO) {
            onDeviceAddingProgressStateChanged(true)
            deviceRepository.addDevice(
                Device(
                    UUID.randomUUID().toString()
                )
            )
        }.invokeOnCompletion {
            onServerUrlStateChanged(sharedPreferences.getString(SERVER_URL_KEY, "")?: "")
            onDeviceAddingProgressStateChanged(false)
            startFileSync()
        }
    }

    /**
     * The remove device event handler
     * @author MaskedRedstonerProZ
     */
    fun removeDevice() {
        viewModelScope.launch {
            deviceRepository.removeDevice(
                sharedPreferences.getString(DEVICE_ID_KEY, "error") ?: "error"
            )
        }.invokeOnCompletion {
            if(it == null) {
                viewModelScope.launch(Dispatchers.IO) {
                    fileRepository.clearLocalDatabase()
                    fileSyncStartWorkerManager.cancelWork()
                    fileSyncWorkerManager.cancelWork()
                }
                fileChangeAddWorkerManager.cancelWork()
            }
        }
    }
}