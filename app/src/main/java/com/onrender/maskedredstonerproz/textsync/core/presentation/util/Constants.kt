package com.onrender.maskedredstonerproz.textsync.core.presentation.util

/**
 * The general constant definition object
 * @author MaskedRedstonerProZ
 */
object Constants {

    /**
     * The debug log tag constant
     * @author MaskedRedstonerProZ
     */
    const val DEBUG_TAG = "TextPad"

    /**
     * The email tag constant
     * @author MaskedRedstonerProZ
     */
    const val EMAIL_TAG = "mail"

    /**
     * The url tag constant
     * @author MaskedRedstonerProZ
     */
    const val URL_TAG = "url"

    /**
     * The server url shared preferences key
     * @author MaskedRedstonerProZ
     */
    const val SERVER_URL_KEY = "server_url"

    /**
     * The device id shared preferences key
     * @author MaskedRedstonerProZ
     */
    const val DEVICE_ID_KEY = "device_id"

    /**
     * The database name constant
     * @author MaskedRedstonerProZ
     */
    const val DATABASE_NAME = "textsync-db"

    /**
     * The file sync start work name constant
     * @author MaskedRedstonerProZ
     */
    const val FILE_SYNC_START_WORK_NAME = "file_sync_start"

    /**
     * The file sync work name constant
     * @author MaskedRedstonerProZ
     */
    const val FILE_SYNC_WORK_NAME = "file_sync"

    /**
     * The file change add work name constant
     * @author MaskedRedstonerProZ
     */
    const val FILE_CHANGE_ADD_WORK_NAME = "file_add"

    /**
     * The device id query parameter constant
     * @author MaskedRedstonerProZ
     */
    const val PARAMETER_DEVICE_ID = "device_id"

    /**
     * The http route constant definition object
     * @author MaskedRedstonerProZ
     */
    object HttpRoutes {

        /**
         * The device adding route constant
         * @author MaskedRedstonerProZ
         */
        const val ROUTE_DEVICE_ADD = "/api/device/add"

        /**
         * The device removing route constant
         * @author MaskedRedstonerProZ
         */
        const val ROUTE_DEVICE_REMOVE = "/api/device/remove"

        /**
         * The file adding route constant
         * @author MaskedRedstonerProZ
         */
        const val ROUTE_FILE_ADD = "/api/file/add"

        /**
         * The file removing route constant
         * @author MaskedRedstonerProZ
         */
        const val ROUTE_FIlE_REMOVE = "/api/file/remove"

        /**
         * The file sync starting route constant
         * @author MaskedRedstonerProZ
         */
        const val ROUTE_FIlE_SYNC_START = "/api/file/sync/start"

        /**
         * The file sync stopping route constant
         * @author MaskedRedstonerProZ
         */
        const val ROUTE_FIlE_SYNC_STOP = "/api/file/sync/start"

        /**
         * The file sync route constant
         * @author MaskedRedstonerProZ
         */
        const val ROUTE_FIlE_SYNC = "/api/file/sync"

    }

    /**
     * The notification channel id constant definition object
     * @author MaskedRedstonerProZ
     */
    object NotificationChannelIDs {

        /**
         * The file change add notification channel id constant
         * @author MaskedRedstonerProZ
         */
        const val FILE_CHANGE_ADD_NOTIFICATION_CHANNEL_ID = "file_change_add"

        /**
         * The file sync notification channel id constant
         * @author MaskedRedstonerProZ
         */
        const val FILE_SYNC_NOTIFICATION_CHANNEL_ID = "file_sync"

    }

    /**
     * The Work Manager work result data key constant definition object
     * @author MaskedRedstonerProZ
     */
    object WorkerKeys {

        /**
         * The error message work result data key constant
         * @author MaskedRedstonerProZ
         */
        const val ERROR_MESSAGE_KEY = "errorMsg"

        /**
         * The api response work result data key constant
         * @author MaskedRedstonerProZ
         */
        const val API_RESPONSE_KEY = "apiResponse"

    }
}