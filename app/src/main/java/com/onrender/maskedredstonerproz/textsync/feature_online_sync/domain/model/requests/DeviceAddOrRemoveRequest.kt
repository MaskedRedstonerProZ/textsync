package com.onrender.maskedredstonerproz.textsync.feature_online_sync.domain.model.requests

import kotlinx.serialization.Serializable

/**
 * Request data class that represents a device upload or deletion request
 * @param deviceId The id of the device being uploaded or deleted
 * @author MaskedRedstonerProZ
 */
@Serializable
data class DeviceAddOrRemoveRequest(
    val deviceId: String
)
