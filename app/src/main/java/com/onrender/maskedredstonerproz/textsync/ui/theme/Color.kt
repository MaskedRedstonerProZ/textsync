package com.onrender.maskedredstonerproz.textsync.ui.theme

import androidx.compose.ui.graphics.Color

/**
 * The project's dark gray colour
 * @author MaskedRedstonerProZ
 */
val darkGray = Color(0xFF30343F)
/**
 * The project's black colour
 * @author MaskedRedstonerProZ
 */
val black = Color(0xFF000000)
/**
 * The project's white colour
 * @author MaskedRedstonerProZ
 */
val white = Color(0xFFF7F0F5)
/**
 * The project's red colour
 * @author MaskedRedstonerProZ
 */
val red = Color(0xFFC80000)
/**
 * The project's blue colour
 * @author MaskedRedstonerProZ
 */
val blue = Color(0xFF6184D8)