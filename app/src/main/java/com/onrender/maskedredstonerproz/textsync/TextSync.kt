package com.onrender.maskedredstonerproz.textsync

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.NotificationChannelIDs.FILE_CHANGE_ADD_NOTIFICATION_CHANNEL_ID
import com.onrender.maskedredstonerproz.textsync.core.presentation.util.Constants.NotificationChannelIDs.FILE_SYNC_NOTIFICATION_CHANNEL_ID
import com.onrender.maskedredstonerproz.textsync.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * The application class
 * @author MaskedRedstonerProZ
 */
class TextSync : Application() {

    override fun onCreate() {
        super.onCreate()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            /**
             * File change adding notification channel
             * @author MaskedRedstonerProZ
             */
            val fileChangeAddChannel = NotificationChannel(
                FILE_CHANGE_ADD_NOTIFICATION_CHANNEL_ID,
                getString(R.string.file_change_add_notification_channel_name),
                NotificationManager.IMPORTANCE_HIGH
            )

            /**
             * File syncing notification channel
             * @author MaskedRedstonerProZ
             */
            val fileSyncChannel = NotificationChannel(
                FILE_SYNC_NOTIFICATION_CHANNEL_ID,
                getString(R.string.file_sync_notification_channel_name),
                NotificationManager.IMPORTANCE_HIGH
            )

            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
                createNotificationChannel(fileChangeAddChannel)
                createNotificationChannel(fileSyncChannel)
            }
        }

        startKoin {

            androidLogger(Level.ERROR)

            androidContext(this@TextSync)

            modules(
                listOf(
                    appModule
                )
            )

        }
    }
}