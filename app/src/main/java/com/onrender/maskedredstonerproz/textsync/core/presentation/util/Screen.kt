package com.onrender.maskedredstonerproz.textsync.core.presentation.util

/**
 * The class that defines what info each [Screen] is supposed to have
 * @param route The screen's route
 * @param name The screen's name
 * @author MaskedRedstonerProZ
 */
sealed class Screen(private val route: String, val name: String) {

    /**
     * Main Screen info object
     * @author MaskedRedstonerProZ
     */
    object MainScreen: Screen("main_screen", "Home")

    /**
     * Online Sync Screen info object
     * @author MaskedRedstonerProZ
     */
    object OnlineSyncManagementScreen: Screen("online_sync_management_screen", "Online Sync")

    /**
     * About Screen info object
     * @author MaskedRedstonerProZ
     */
    object AboutScreen: Screen("about_screen", "About")

    /**
     * Invoke method that return's the screen's route when the [Screen] is invoked like a method
     * @return The screen's route
     * @author MaskedRedstonerProZ
     */
    open operator fun invoke() = this.route
}
