package com.onrender.maskedredstonerproz.textsync.feature_text_editing.presentation.components

import androidx.compose.material.DrawerState
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import com.onrender.maskedredstonerproz.textsync.R
import com.onrender.maskedredstonerproz.textsync.core.presentation.components.AutoResizedText

/**
 * The standard app bar that is shown on every screen
 * @param modifier The modifier
 * @param scope The coroutine scope
 * @param drawerState The drawer state
 * @param options Icons of different actions on a particular screen
 * @author MaskedRedstonerProZ
 */
@Composable
fun StandardAppBar(
    modifier: Modifier,
    scope: CoroutineScope,
    drawerState: DrawerState,
    options: (@Composable () -> Unit)? = null
) {
    TopAppBar(
        title = {
            AutoResizedText(text = stringResource(id = R.string.app_name))
        },
        modifier = modifier,
        navigationIcon = {
            IconButton(onClick = {

                scope.launch {
                    drawerState.open()
                }
            }
            ) {
                Icon(Icons.Default.Menu, "menu")
            }
        },
        backgroundColor = MaterialTheme.colors.onBackground,
        contentColor = MaterialTheme.colors.primaryVariant,
        actions = {
            if (options == null) {
                return@TopAppBar
            }

            options()
        }
    )
}
