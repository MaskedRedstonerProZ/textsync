package com.onrender.maskedredstonerproz.textsync.feature_online_sync.presentation.screens.file_list_screen

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel

class PermissionHandlingViewModel: ViewModel() {

    private val _visiblePermissionDialogQueue = mutableStateListOf<String>()
    val visiblePermissionDialogQueue = _visiblePermissionDialogQueue.reversed()

    fun dismissDialog() {
        _visiblePermissionDialogQueue.removeLast()
    }

    fun onPermissionResult(permission: String, isGranted: Boolean, onPermissionGranted: (it: String) -> Unit) {
        if (!isGranted) {
            _visiblePermissionDialogQueue.add(0, permission)
            onPermissionGranted(permission)
        }
    }

}